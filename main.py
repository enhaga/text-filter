from typing import Optional
from fastapi import FastAPI
from better_profanity import profanity

from detoxify import Detoxify
import spacy

app = FastAPI()

custom_badwords = ['suck', 'asshole', 'assholes', 'Loser',
                   'shit', 'fucking', 'Fuck', 'dumbest',
                   'bitch',  'fucking', 'die', 'killed',
                   'rape', 'kill']
profanity.add_censor_words(custom_badwords)

# Endpoints
@app.get("/")
def read_root():
    return "ENHAGA censor server is running!"

@app.get("/censor/{text}")
def censor(text:str):
    return profanity.censor(text)

@app.get("/toxicity/{text}")
async def toxicity(text:str):
    weights =  Detoxify('original').predict(text)
    print(weights)
    ret = {}
    for i in weights:
        ret[i] = float(weights[i])
    return ret 

