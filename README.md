# ENHAGA Text Filter
The ENHAGA text filter is a web-based app for fitering inappropriate text. It provides profanity censonrship (using pypi.org/project/better-profanity/) and toxic text prediction (via github.com/unitaryai/detoxify)

# Installation
- Install package dependencies by running `pip install requirements.txt`
- Start the server by running `uvicorn main:app --reload `

# Usage

### Profanity filter:

Make a GET request to `http://127.0.0.1:8000/censor/[text to filter]`. This will return the text with profanities overwritten with astirisks.


### Toxic text Prediction:

Make a GET request to `http://127.0.0.1:8000/toxicity/[text to check]`. This will return a JSON dictionary of scores that identify the nature of the text. This is useful for identifying text that is offensive but does not contain profanities. 

Examples:

`http://127.0.0.1:8000/toxicity/get cancer`

```
toxicity	0.9409715533256531
severe_toxicity	0.03556308522820473
obscene	0.0346713624894619
threat	0.41614946722984314
insult	0.17207196354866028
identity_attack	0.043770913034677505
```

`http://127.0.0.1:8000/toxicity/I have cancer`
```
toxicity	0.10376545786857605
severe_toxicity	0.0003712562029249966
obscene	0.0029271256644278765
threat	0.0010575251653790474
insult	0.0011207002680748701
identity_attack	0.000926608161535114
```

